import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  imageUrl = 'https://img.freepik.com/premium-photo/scientific-molecule-background-with-flow-waves-medicine-science-technology-chemistry-wallpaper-o_230610-906.jpg';

  isInvisibleRandomArray = false;
  isInvisibleBubbleSorted = false;
  isInvisibleSelectionSorted = false;
  isInvisibleInsertionSorted = false;
  isInvisibleMergeSorted = false;
  isInvisibleQuickSorted = false;


  randomArray: string[] = [];
  bubbleSortedArray: string[] = [];
  selectionSortedArray: string[] = [];
  insertionSortedArray: string[] = [];
  mergeSortedArray: string[] = [];
  quickSortedArray: string[] = [];


  genString(length: any) {
    const characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    let result = '';
    for (let i = 0; i < length; i++) {
      const index = Math.floor(Math.random() * characters.length);
      result += characters.charAt(index);
    }
    return result;
  }

  genArray() {
    this.isInvisibleRandomArray = true;
    for (let i = 0; i < 1000; i++) {
      const randomLength = Math.floor(Math.random() * 5) + 1;
      const randomString = this.genString(randomLength);
      this.randomArray.push(randomString);
    }
    this.bubbleSortedArray = this.randomArray;
    this.selectionSortedArray = this.randomArray;
    this.insertionSortedArray = this.randomArray;
    this.mergeSortedArray = this.randomArray;
    this.quickSortedArray = this.randomArray;
  }

  //Bubble Sort: Sắp xếp nổi bọt
  bubbleSort() {
    this.isInvisibleBubbleSorted = true;
    const length = this.bubbleSortedArray.length;
    for (let i = 0; i < length - 1; i++) {
      for (let j = 0; j < length - 1 - i; j++) {
        if (this.bubbleSortedArray[j] > this.bubbleSortedArray[j + 1]) {
          [this.bubbleSortedArray[j], this.bubbleSortedArray[j + 1]] = [this.bubbleSortedArray[j + 1], this.bubbleSortedArray[j]];
        }
      }
    }
    return this.bubbleSortedArray;
  }

  //Selection Sort: Sắp xếp chọn
  selectionSort() {
    this.isInvisibleSelectionSorted = true;
    const length = this.selectionSortedArray.length;
    for (let i = 0; i < length - 1; i++) {
      let minIndex = i;
      for (let j = i + 1; j < length; j++) {
        if (this.selectionSortedArray[j] < this.selectionSortedArray[minIndex]) {
          minIndex = j;
        }
      }
      if (minIndex !== i) {
        [this.selectionSortedArray[i], this.selectionSortedArray[minIndex]] = [this.selectionSortedArray[minIndex], this.selectionSortedArray[i]];
      }
    }
  }

  //Insertion Sort: Sắp xếp chèn
  insertionSort() {
    this.isInvisibleInsertionSorted = true;
    const length = this.insertionSortedArray.length;
    for (let i = 1; i < length; i++) {
      const key = this.insertionSortedArray[i];
      let j = i - 1;
      while (j >= 0 && this.insertionSortedArray[j] > key) {
        this.insertionSortedArray[j + 1] = this.insertionSortedArray[j];
        j--;
      }
      this.insertionSortedArray[j + 1] = key;
    }
  }

  // Merge Sort: Sắp xếp trộn
  mergeSort() {
    debugger
    this.isInvisibleMergeSorted = true;
    const result: string[] = [];
    const mid = Math.floor(this.mergeSortedArray.length / 2);
    const left = this.mergeSortedArray.slice(0, mid);
    const right = this.mergeSortedArray.slice(mid);

    while (left.length && right.length) {
      if (left[0] <= right[0]) {
        result.push(left.shift()!);
      } else {
        result.push(right.shift()!);
      }
    }

    while (left.length) {
      result.push(left.shift()!);
    }

    while (right.length) {
      result.push(right.shift()!);
    }
    debugger
    this.mergeSortedArray = result;
  }


  //Quick Sort: Sắp xếp nhanh

   quickSort(array: string[]): string[] {
    this.isInvisibleQuickSorted = true;
    if (array.length <= 1) {
      return array;
    }

    const pivotIndex = Math.floor(Math.random() * array.length);
    const pivot = array[pivotIndex];
    const left: string[] = [];
    const right: string[] = [];

    for (let i = 0; i < array.length; i++) {
      if (i === pivotIndex) continue;
      if (array[i] < pivot) {
        left.push(array[i]);
      } else {
        right.push(array[i]);
      }
    }

    return [...this.quickSort(left), pivot, ...this.quickSort(right)];
  }

  quickSort1() {
    debugger;
    this.quickSortedArray = this.quickSort(this.randomArray);
    console.log(this.quickSortedArray);
  }

}
